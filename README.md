# THIS REPOSITORY IS DEPRECATED!

The demos should still work with newer versions of AMBRose, but the latest
version of AMBRose is now stored on the Rosetta Github, at
[tools/AmbRose](https://github.com/RosettaCommons/tools/tree/master/AmbRose).

If you don't have permission to be on the Rosetta Github (but your copy of
PyRosetta is legal, right?) then you can still view the AMBRose documentation
[here](https://www.rosettacommons.org/docs/latest/AMBRose).