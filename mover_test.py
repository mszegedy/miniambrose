import pyrosetta as pr; pr.init()
import ambrose

pose = pr.pose_from_file('protein-test/apo-1jp9-original.pdb')
mover = ambrose.AMBERSimulateMover(working_dir='mover-test',
                                   prefix='dry-sim-norefc',
                                   solvent=None)
mover.output_interval = 0.1 # every 0.1 ps
mover.duration = 0.5        # 0.5 ps; 500 steps, 5 frames
mover.temperature = 273.    # 273 K
mover.starting_temperature = 273.
mover.seed = 0
mover.mdin_dict['ntpr'] = 10 # every 0.01 ps = 10 fs = 10 steps
print('Explicit working_dir, dry, simulation, no refc.', flush=True)
mover.apply(pose)
pose.dump_pdb('mover-test/dry-sim-norefc-result.pdb')
print('Explicit working_dir, wet, simulation, no refc.', flush=True)
mover.prefix = 'wet-sim-norefc'
mover.solvent = ambrose.Solvent.SPCE_WATER
mover.apply(pose)
pose.dump_pdb('mover-test/wet-sim-norefc-result.pdb')
