import pyrosetta as pr; pr.init()
import pytraj as pt
import ambrose


def test_protein():
    print('\n\nTesting protein import...\n\n')
    traj = pt.iterload('protein-test/psMb-293.mdcrd',
                       'protein-test/psMb-293.prmtop')
    scorefxn = pr.get_fa_scorefxn()
    minmover = pr.rosetta.protocols.relax.FastRelax(scorefxn, 0)
    poses = ambrose.TrajToPoses(traj)
    pose = poses[0]
    print('\n\nProtein imported successfully. FastRelaxing frame 0...')
    print('    Prior score:', scorefxn.score(pose), '\n\n')
    pose.dump_pdb('protein-test/out.pdb')
    minmover.apply(pose)
    print('\n\nPosterior score:', scorefxn.score(pose), '\n\n')
    pose.dump_pdb('protein-test/out-minimized.pdb')

def test_rna():
    print('\n\nTesting RNA import...\n\n')
    traj = pt.iterload('rna-test/tiny.mdcrd',
                       'rna-test/tiny.prmtop')
    scorefxn = pr.get_fa_scorefxn()
    minmover = pr.rosetta.protocols.rna.denovo.movers.RNA_Minimizer()
    poses = ambrose.TrajToPoses(traj)
    pose = poses[0]
    print('\n\nRNA imported successfully. Minimizing frame 0...')
    print('    Prior score:', scorefxn.score(pose), '\n\n')
    pose.dump_pdb('rna-test/out.pdb')
    minmover.apply(pose)
    print('\n\nPosterior score:', scorefxn.score(pose), '\n\n')
    pose.dump_pdb('rna-test/out-minimized.pdb')

if __name__ == '__main__':
    test_protein()
    test_rna()
