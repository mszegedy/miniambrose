import subprocess
import pyrosetta as pr
import pytraj as pt
import ambrose
import os
import sys
from mpi4py import MPI
MPICOMM = MPI.COMM_WORLD
MPIRANK = MPICOMM.Get_rank()
MPISIZE = MPICOMM.Get_size()

if MPIRANK != 0:
    sys.exit()
pr.init()

pose = pr.pose_from_file('protein-test/apo-1jp9-original.pdb')
os.chdir('md-test')
ambrose.pose_to_amber_params(pose,
                             'input.rst7',
                             'topology.parm7')
print('Attempting MD...', flush=True)
MPI.COMM_SELF.Spawn(os.path.join(ambrose.amber_bin(), 'sander.MPI'),
                    args=['-O', '-i', '0.in', '-c', 'input.rst7', '-p',
                          'topology.parm7', '-r', '0.rst7'],
                    maxprocs=MPISIZE)
MPI.COMM_SELF.Spawn(os.path.join(ambrose.amber_bin(), 'sander.MPI'),
                    args=['-O', '-i', '1.in', '-c', '0.rst7', '-p',
                          'topology.parm7', '-x', '1.nc'],
                    maxprocs=MPISIZE)
MPI.COMM_SELF.Spawn(sys.executable, 'example_mpi.py', maxprocs=MPISIZE)
print('Finished MD!', flush=True)
traj = pt.iterload('1.nc', 'topology.parm7')
scorefxn = pr.get_fa_scorefxn()
poses = ambrose.TrajToPoses(traj)
for index, pose in enumerate(poses):
    print('Frame {:>3}: {:8.3f}'.format(index, scorefxn.score(pose)))
