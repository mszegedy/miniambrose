import pyrosetta as pr; pr.init()

def heavy_atom_mask(pose):
    mask = pr.rosetta.core.id.AtomID_Map_bool()
    mask.default_value(True)
    size = pose.size()
    mask.resize(size)
    for i in range(1, size+1):
        r = pose.residue(i)
        natoms = r.natoms()
        mask.resize(i, natoms)
        nheavyatoms = r.nheavyatoms()
        for j in range(1, natoms+1):
            if j > nheavyatoms:
                mask.set(pr.AtomID(j, i), False)
    return mask

pose = pr.pose_from_file('apo-1jp9-original.pdb')
fstream = pr.rosetta.std.ofstream('test_2.pdb',
                                  pr.rosetta.std._Ios_Openmode._S_out)
pr.rosetta.core.io.pdb.dump_pdb(pose, fstream, heavy_atom_mask(pose))
