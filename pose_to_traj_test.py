import pyrosetta as pr; pr.init()
import ambrose

pose = pr.pose_from_file('protein-test/apo-1jp9-original.pdb')
ambrose.pose_to_amber_params(pose,
                             'leap-test/out.rst7',
                             'leap-test/out.parm7',
                             'leap-test/out.log',
                             'leap-test/out.in',
                             solvent='SPCBOX',
                             solvent_shape='box',
                             add_ions=False)
